package ExercisesLecture4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DropdownTest {

    WebDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://pragmatic.bg/automation/lecture13/Config.html");
    }

    @AfterMethod
    public void closeBrowser() {
        driver.quit();
    }

    @Test
    public void dropdownTest() throws InterruptedException {
        WebElement dropdown = driver.findElement(By.name("make"));
        Select make = new Select(dropdown);
        Assert.assertFalse(make.isMultiple());
        Assert.assertEquals(make.getOptions().size(), 4);

        List<String> exp_options = Arrays.asList(new String[]{"BMW", "Mercedes", "Audi", "Honda"});
        List<String> actual_options = new ArrayList<String>();

        List<WebElement> allOptions = make.getOptions();
        for (WebElement option : allOptions) {
            actual_options.add(option.getText());
        }
        Assert.assertEquals(actual_options.toArray(), exp_options.toArray());

        make.selectByVisibleText("Honda");
        Assert.assertEquals(make.getFirstSelectedOption().getText(), "Honda");

        Thread.sleep(3000);

        make.selectByValue("audi");
        Assert.assertEquals(make.getFirstSelectedOption().getText(), "Audi");

        Thread.sleep(3000);

        make.selectByIndex(0);
        Assert.assertEquals(make.getFirstSelectedOption().getText(), "BMW");

        Thread.sleep(3000);
    }

    @Test
    public void dropdownTestMultipleSelection() throws InterruptedException {
        Select color = new Select(driver.findElement(By.name("color")));
        Assert.assertTrue(color.isMultiple());
        Assert.assertEquals(color.getOptions().size(), 5);
        color.selectByVisibleText("Black");
        Thread.sleep(3000);
        color.selectByVisibleText("Red");
        Thread.sleep(3000);
        color.selectByVisibleText("Silver");
        Thread.sleep(3000);
        Assert.assertEquals(color.getAllSelectedOptions().size(), 3);
        List<String> exp_sel_opitions = Arrays.asList(new String[]{"Black", "Red", "Silver"});
        List<String> actual_sel_options = new ArrayList<String>();

        for (WebElement option : color.getAllSelectedOptions()){
            actual_sel_options.add(option.getText());
        }

        Assert.assertEquals(actual_sel_options.toArray(), exp_sel_opitions.toArray());

        color.deselectByVisibleText("Silver");
        Assert.assertEquals(color.getAllSelectedOptions().size(), 2);

        color.selectByValue("rd");
        Assert.assertEquals(color.getAllSelectedOptions().size(), 1);

        color.deselectByIndex(0);
        Assert.assertEquals(color.getAllSelectedOptions().size(), 0);















    }
}