package ExercisesLecture4;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ActionClassExercises {
    public static WebDriver driver;

    @BeforeMethod
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://pragmatic.bg/automation/lecture13/Config.html");
    }

    @AfterMethod
    public void closeBrowser(){
        driver.quit();
    }

    @Test
    public void multipleSelectTest() {
        Select allColors = new Select(driver.findElement(By.name("color")));

        Actions builder = new Actions(driver);

        builder.keyDown(Keys.CONTROL);
        builder.click(driver.findElement(By.cssSelector("select[name='color'] option[value='bl']")));
        builder.click(driver.findElement(By.cssSelector("select[name='color'] option[value='rd']")));
        builder.click(driver.findElement(By.cssSelector("select[name='color'] option[value='sl']")));
        builder.keyUp(Keys.CONTROL);
        builder.perform();

        Assert.assertEquals(allColors.getAllSelectedOptions().size(), 3);

        List<String> exp_sel_options = Arrays.asList(new String[]{"Black", "Red", "Silver"});
        List<String> act_sel_options = new ArrayList<String>();

        for (WebElement option : allColors.getAllSelectedOptions()) {
            act_sel_options.add(option.getText());
        }
        Assert.assertEquals(exp_sel_options, act_sel_options);
    }
}
