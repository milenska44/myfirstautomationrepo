package inheritance;

/**
 * The class represents and object, named Person, its' fields, constructors and methods.
 * This class is the parent class of Employee and Student classes.
 */
public class Person {
    private String name;
    private int age;
    private boolean isMale;

    public Person() {
    }

    public Person(String name, int age, boolean isMale) {
        this.name = name;
        this.age = age;
        this.isMale = isMale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    /**
     * The method represents information about each person (name, age and and gender (isMan = true).
     *
     * @return
     */

    public String showPersonInfo() {
        System.out.println(toString());
        return toString();

    }

    /**
     * The method shows all personal data about each Person.
     *
     * @return
     */
    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", isMale=" + isMale +
                '}';
    }
}
