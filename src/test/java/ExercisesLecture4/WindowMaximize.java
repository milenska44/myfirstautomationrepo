package ExercisesLecture4;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class WindowMaximize {

    public static WebDriver driver;

    @Test
    public void testRowSelectionUsingControlKey (){
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("learn.pragmatic.com");
        driver.quit();
    }
}
