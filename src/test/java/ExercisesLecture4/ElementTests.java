package ExercisesLecture4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ElementTests {
    WebDriver driver;

    @BeforeMethod
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://pragmatic.bg/automation/lecture13/DoubleClickDemo.html");
    }

    @Test
    public void testElement(){
        WebElement message = driver.findElement(By.id("message"));
        Assert.assertEquals(message.getText(),
                "Click on me and my color will change",
                "this will appear if expected and actual are not the same");
        Assert.assertTrue(message.getText().contains("color"), "nqma q be, bug");
    }
}
