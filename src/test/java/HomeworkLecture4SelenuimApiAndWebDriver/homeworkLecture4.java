package HomeworkLecture4SelenuimApiAndWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class homeworkLecture4 {

    public static WebDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg/admin");
    }

    @AfterMethod
    public void closeBrowser() {
        driver.quit();
    }


    @Test
    public void positiveTestLoginAndDropdown() throws InterruptedException {
        WebElement loginUsername = driver.findElement(By.id("input-username"));
        loginUsername.sendKeys("admin");
        WebElement loginPassword = driver.findElement(By.id("input-password"));
        loginPassword.sendKeys("parola123!");
        WebElement buttonSubmit = driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
        buttonSubmit.click();
        Thread.sleep(3);
        Assert.assertEquals(driver.findElement(By.xpath("//span[contains(@class,'hidden-xs')]")).getAttribute("class"),
                "hidden-xs hidden-sm hidden-md");

        WebElement sales = driver.findElement(By.xpath("//li[@id='menu-sale']/a[contains(@href,'#collapse')]"));
        sales.click();
        Thread.sleep(3000);
        WebElement orders = driver.findElement(By.xpath("//li[@id='menu-sale']//a[contains(@href,'sale/order')]"));
        orders.click();
        Thread.sleep(3000);

        WebElement dropDown = driver.findElement(By.name("filter_order_status_id"));
        Select filter_order_status_id = new Select(dropDown);

        Assert.assertFalse(filter_order_status_id.isMultiple());
        Assert.assertEquals(filter_order_status_id.getOptions().size(), 16);

        List<String> expected_options = Arrays.asList(new String[]{"", "Missing Orders", "Canceled", "Canceled Reversal",
                "Chargeback", "Complete", "Denied", "Expired", "Failed", "Pending", "Processed", "Processing", "Refunded",
                "Reversed", "Shipped", "Voided"});
        List<String> actual_options = new ArrayList<String>();

        List<WebElement> allOptions = filter_order_status_id.getOptions();

        for (WebElement option : allOptions) {
            actual_options.add(option.getText());
        }

        Assert.assertEquals(expected_options.toArray(), actual_options.toArray());

        filter_order_status_id.selectByVisibleText("Processed");
        Assert.assertEquals(filter_order_status_id.getFirstSelectedOption().getText(), "Processed");
        Thread.sleep(5000);

        filter_order_status_id.selectByIndex(15);
        Assert.assertEquals(filter_order_status_id.getFirstSelectedOption().getText(), "Voided");
        Thread.sleep(5000);

        filter_order_status_id.selectByValue("10");
        Assert.assertEquals(filter_order_status_id.getFirstSelectedOption().getText(), "Failed");

        WebElement logoutButton = driver.findElement(By.xpath("//span[contains(@class,'hidden-xs')]"));
        logoutButton.click();
    }

    @Test
    public void negativeTestLoginWithWrongUsername() throws InterruptedException {
        WebElement loginUsername = driver.findElement(By.id("input-username"));
        loginUsername.sendKeys("admin123");
        WebElement loginPassword = driver.findElement(By.id("input-password"));
        loginPassword.sendKeys("parola123!");
        WebElement buttonSubmit = driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
        buttonSubmit.click();

        Thread.sleep(3);

        WebElement buttonSubmitSecondAttempt = driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
        Assert.assertEquals(buttonSubmitSecondAttempt.getAttribute("class"), "btn btn-primary");

        Thread.sleep(3000);
    }

    @Test
    public void negativeTestLoginWithWrongPassword() throws InterruptedException {
        WebElement loginUsername = driver.findElement(By.id("input-username"));
        loginUsername.sendKeys("admin");
        WebElement loginPassword = driver.findElement(By.id("input-password"));
        loginPassword.sendKeys("parola");
        WebElement buttonSubmit = driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
        buttonSubmit.click();

        Thread.sleep(3);

        WebElement buttonSubmitSecondAttempt = driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
        Assert.assertEquals(buttonSubmitSecondAttempt.getAttribute("class"), "btn btn-primary");

        Thread.sleep(3000);
    }

    @Test
    public void negativeTestLoginWithWrongUsernameAndPassword() throws InterruptedException {
        WebElement loginUsername = driver.findElement(By.id("input-username"));
        loginUsername.sendKeys("admin");
        WebElement loginPassword = driver.findElement(By.id("input-password"));
        loginPassword.sendKeys("parola");
        WebElement buttonSubmit = driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
        buttonSubmit.click();

        Thread.sleep(3);

        WebElement buttonSubmitSecondAttempt = driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
        Assert.assertEquals(buttonSubmitSecondAttempt.getAttribute("class"), "btn btn-primary");

        Thread.sleep(3000);
    }
}
