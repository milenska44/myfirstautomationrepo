package inheritance;

/**
 * The class represents the object Student with its' fields, constructors and methods.
 */
public class Student extends Person {
    double score;

    public Student() {

    }

    public Student(String name, int age, boolean isMale, double score) {
        super(name, age, isMale);
        this.score = score;
    }

    /**
     * The method represents personal data for each Student and its' score during its' education.
     *
     * @return
     */
    public String showStudentInfo() {
        System.out.println("The score of " + getName() + " is " + score);
        return getName() + score;
    }


}
