package inheritance;

public class Demo {
    public static void main(String[] args) {
        Person[] person = new Person[10];

        Person personN = new Student();
//
//        Person firstPerson = new Person("Ivan", 25, true);
//        Person secondPerson = new Person("Stoyan", 30, true);

//        Student firstStudent = new Student("Mariya", 19, false, 5.5);
//        Student secondStudent = new Student("Nasko", 16, true, 4.6);

//        Employee firstEmployee = new Employee("Kircho", 18, true, 60.00);
//        Employee secondEmployee = new Employee("Slavka", 64, false, 55.50);
//        firstPerson.showPersonInfo();
//        firstEmployee.calculateOvertime(8);
//        secondEmployee.showEmployeeInfo();

        person[0] = new Person("Ivan", 25, true);
        person[1] = new Person("Stoyan", 30, true);
        person[2] = new Student("Mariya", 19, false, 5.5);
        person[3] = new Student("Nasko", 16, true, 4.6);
        person[4] = new Employee("Kircho", 18, true, 60.00);
        person[5] = new Employee("Slavka", 64, false, 55.50);

        for (int i = 0; i < person.length; i++) {
            if (person[i] instanceof Person) {
                person[i].showPersonInfo();
            }
            if (person[i] instanceof Student) {
                ((Student) person[i]).showStudentInfo();
            }
            if (person[i] instanceof Employee) {
                ((Employee) person[i]).showEmployeeInfo();
            }
        }
        for (int i = 0; i < person.length; i++) {
            if (person[i] instanceof Employee) {
                ((Employee) person[i]).calculateOvertime(10);
            }
        }
    }
}
