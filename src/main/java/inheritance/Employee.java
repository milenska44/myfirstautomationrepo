package inheritance;

/**
 * This class represents an object named Employee with its' fields, constructors and methods.
 */
public class Employee extends Person {
    double daySalary;


    public Employee(String name, int age, boolean isMale, double daySalary) {
        super(name, age, isMale);
        this.daySalary = daySalary;
    }

    /**
     * The method calculates the overtime for each Employee.
     *
     * @param hours
     * @return
     */
    public double calculateOvertime(double hours) {
        double overtime = 0;
        double overtimeSum = 0;
        double sumPerHour = 0;
        if (getAge() < 18) {
            System.out.println(getName() + " can not work!!!");
        }
        if (getAge() >= 18 && hours <= 8) {
            System.out.println(getName() + " did't work more than 8 hours.");
        }
        if (getAge() >= 18 && hours > 8) {
            sumPerHour = daySalary / 8;
            overtime = hours - 8;
            overtimeSum = (sumPerHour * overtime * 1.5);
            System.out.println(getName() + " worked " + overtime + " hours overtime and have to be paid "
                    + overtimeSum + " leva more or " + (daySalary + overtimeSum) + " leva per whole day! ");
            return overtimeSum;
        }
        return overtimeSum;
    }

    /**
     * The method represents Employees' info plus daysalary.
     *
     * @return
     */
    @Override
    public String toString() {
        return "Employee{" +
                "daySalary=" + daySalary +
                "} " + super.toString();
    }

    /**
     * The method represents personal information about each Employee including his day salary.
     *
     * @return
     */
    public String showEmployeeInfo() {
        System.out.println(toString());

        return toString();

    }
}