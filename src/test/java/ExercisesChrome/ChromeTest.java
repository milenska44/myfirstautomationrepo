package ExercisesChrome;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ChromeTest {
    public static WebDriver driver;

    @BeforeMethod
    public void setUp (){
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
        driver.get("http://shop.pragmatic.bg/");
    }

    @Test
            public void testOne (){
        WebElement userLogin = driver.findElement(By.cssSelector("#search input"));
        userLogin.sendKeys("pragmabg");
    }

    @AfterMethod
    public void closeBrowser (){
        driver.quit();
    }




}
